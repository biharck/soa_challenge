# Bem vindo! #

O objetivo deste projeto é resolver um problema de importação de dados bancários.

Um projeto inicial foi criado e está disponível neste repositório.

**Você deve:**
	
1. Formatar uma estrutura de dados de entrada contendo nome do cliente, código da conta, débito/crédito, data de transação, código do banco.

2. Usar o Composto BatchReader como responsável por ler este arquivo csv.

3. Após ler este arquivo, o serviço deve validar se o cliente existe [ValidadeAccount] e em seguida atualizar o saldo [updateBalance]

4. Durante todo o processo você deve utilizar o serviço LogEAI para registro de log, tanto de sucesso quanto de falha.

5. O serviço logEAI deve gravar todo o log do processo em um arquivo texto.

Seu objetivo é implementar todas as pontas sem a necessidade de usar banco de dados. 

Portanto, **seja criativo** e utilize os recursos providos pelo SOA Suíte.

Faça um Fork deste projeto e ao terminar, dê permissão para vermos o que você fez.

### Boa sorte! ###